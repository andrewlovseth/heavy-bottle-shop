<?php include(locate_template('partials/header/global-variables.php')); ?>
<!DOCTYPE html>
<html>
<head>
	<?php the_field('head_meta', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" type="text/css" media="all" href="/wp-content/themes/heavy-restaurants/heavy-restaurants.css" />

	<?php get_template_part('partials/head/styles'); ?>
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<?php the_field('body_meta', 'options'); ?>

	<?php if(get_field('show_announcements', 'options') == true): ?>

		<section class="announcement">
			<div class="wrapper">
				
				<div class="info">
					<?php the_field('announcements', 'options'); ?>
				</div>

			</div>
		</section>

	<?php endif; ?>

	<header class="site-header">
		<div class="wrapper">

			<div class="social">			
				<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external">
					<img src="<?php echo $child_theme_path; ?>/images/icon-twitter.svg" alt="Twitter" />
				</a>
				
				<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external">
					<img src="<?php echo $child_theme_path; ?>/images/icon-instagram.svg" alt="Instagram" />
				</a>
			</div>

			<div class="site-logo divider">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<div class="tagline">
					<h3><?php the_field('tagline', 'options'); ?></h3>
				</div>
			</div>
			
		</div>		
	</header>
