<section class="hero">
	<div class="wrapper">
		
		<div class="photo">
			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		<div class="info">
			<div class="headline">
				<h2 class="section-header"><?php the_field('hero_headline'); ?></h2>
			</div>

			<div class="copy p1">
				<?php the_field('hero_copy'); ?>
			</div>				
		</div>

	</div>
</section>