<div class="divider one-third"></div>

<section class="wine-team">
	<div class="wrapper">
		
		<div class="headline">
			<h2 class="section-header"><?php the_field('wine_team_headline'); ?></h2>
		</div>

		<div class="copy p1">
			<?php the_field('wine_team_copy'); ?>
		</div>

		<div class="grid two-col-grid contact-grid">
		 
		    <div class="grid-item contact-item text">
		    	<div class="content">

		    		<div class="flex-item">
			    		<div class="header">
			    			<div class="icon">
			    				<img src="<?php $image = get_field('text_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    			</div>

			    			<div class="headline">
			    				<h3><?php the_field('text_headline'); ?></h3>
			    			</div>			    			
			    		</div>

			    		<div class="contact-info">
			    			<?php if(have_rows('text_contact_info')): while(have_rows('text_contact_info')): the_row(); ?>
							 
							    <div class="entry">
							    	<?php if(get_sub_field('label')): ?>
							    		<h4><?php the_sub_field('label'); ?></h4>
							    	<?php endif; ?>

							    	<p><?php the_sub_field('value'); ?></p>
							    </div>

							<?php endwhile; endif; ?>
			    		</div>
		    		</div>

		    	</div>				        
		    </div>

		    <div class="grid-item contact-item email">
		    	<div class="content">

		    		<div class="flex-item">
			    		<div class="header">
			    			<div class="icon">
			    				<img src="<?php $image = get_field('email_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    			</div>

			    			<div class="headline">
			    				<h3><?php the_field('email_headline'); ?></h3>
			    			</div>			    			
			    		</div>

			    		<div class="contact-info">
			    			<?php if(have_rows('email_contact_info')): while(have_rows('email_contact_info')): the_row(); ?>
							 
							    <div class="entry">
							    	<?php if(get_sub_field('label')): ?>
							    		<h4><?php the_sub_field('label'); ?></h4>
							    	<?php endif; ?>

							    	<p><a href="mailto:<?php the_sub_field('value'); ?>"><?php the_sub_field('value'); ?></a></p>
							    </div>

							<?php endwhile; endif; ?>
			    		</div>


		    		</div>



		    	</div>				        
		    </div>

		</div>
		
	</div>
</section>