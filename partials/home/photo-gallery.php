<section class="grid photo-gallery">
	
	<div class="left-vertical">
		<div class="photo portrait">
			<div class="content">
				<img src="<?php $image = get_field('left_vertical'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>
	</div>

	<div class="middle-top">
		<div class="photo landscape">
			<div class="content">
				<img src="<?php $image = get_field('middle_top_1'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>

		<div class="photo landscape">
			<div class="content">
				<img src="<?php $image = get_field('middle_top_2'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" 	/>
			</div>
		</div>
	</div>

	<div class="middle-bottom">
		<div class="photo panorama">
			<div class="content">
				<img src="<?php $image = get_field('middle_bottom'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		</div>
	</div>

	<div class="right-vertical">
		<div class="photo portrait">
			<div class="content">
				<img src="<?php $image = get_field('right_vertical'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />

			</div>
		</div>
	</div>

</section>