<?php include(locate_template('partials/header/global-variables.php')); ?>

<div class="divider one-third"></div>

<section class="order-online">
	<div class="wrapper">
		
		<div class="headline">
			<h2 class="section-header"><?php the_field('order_online_headline'); ?></h2>
		</div>

		<div class="copy p2">
			<?php the_field('order_online_copy'); ?>
		</div>

		<?php if(have_rows('locations')): ?>

			<div class="grid four-col-grid location-grid">
				<?php while(have_rows('locations')): the_row(); ?>
			 
				    <div class="grid-item location">
				    	<div class="content">

				    		<div class="flex-item">

					    		<div class="logo">
					    			<a href="<?php the_sub_field('location_link'); ?>" rel="external">
					    				<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    			</a>
					    		</div>

					    		<div class="order-now">
									<?php 
										$link = get_sub_field('order_now_link');
										if( $link ): 
										$link_url = $link['url'];
										$link_title = $link['title'];
										$link_target = $link['target'] ? $link['target'] : '_self';
									?>
										<a class="btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
											<span class="outline"><?php echo esc_html( $link_title ); ?></span>
										</a>
									<?php endif; ?>
					    		</div>

					    		<div class="wine-list">
					    			<?php $file = get_sub_field('wine_list'); if( $file ): ?>
						    			<a href="<?php echo $file['url']; ?>" rel="external">
						    				<span class="icon">
						    					<img src="<?php echo $child_theme_path; ?>/images/icon-wine-list.svg" alt="Wine List" />
						    				</span>

						    				<span class="label">View Wine List</span>
						    			</a>
					    			<?php endif; ?>
					    		</div>
				    			
				    		</div>
				    		
				    	</div>				        
				    </div>

				<?php endwhile; ?>
			</div>

		<?php endif; ?>

	</div>
</section>