<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<main class="site-content">
		<?php get_template_part('partials/home/hero'); ?>

		<?php get_template_part('partials/home/order-online'); ?>

		<?php get_template_part('partials/home/wine-team'); ?>

		<?php get_template_part('partials/home/photo-gallery'); ?>		
	</main>

<?php get_footer(); ?>